
[build-system]
build-backend = "poetry.masonry.api"
requires = ["poetry>=1.2.2"]

[project]
name = "ska-sdp-realtime-receive-notebooks"
version = "0.0.1"
description = "Simple notebooks to test aspects of the realtime receive pipeline"
authors = [
    { name = "Stephen Ord", email = "stephen.ord@csiro.au" },
]

[tool.poetry]
name = "ska-sdp-realtime-receive-notebooks"
version = "0.0.1"
description = "Simple notebooks to test aspects of the realtime receive pipeline"
authors = [
    "Stephen Ord <stephen.ord@csiro.au>",
]
license = "BSD-3-Clause"

[[tool.poetry.source]]
name = "skao"
url = "https://artefact.skao.int/repository/pypi-internal/simple"

[[tool.poetry.source]]
name = "core-gitlab"
url = "https://gitlab.com/api/v4/projects/34787134/packages/pypi/simple"

[tool.poetry.dependencies] # until poetry adopts https://peps.python.org/pep-0631/

python = ">=3.10, <3.12" # Has to match scipy requirements
ska-sdp-realtime-receive-core = ">= 6.0.0"
ska-telmodel = ">= 1.7.0"
ipykernel = "^6.24.0"

[tool.poetry.group.lint.dependencies]
black = ">=22.10.0"
flake8 = ">=3.8.4"
isort = ">=5.6.4"
pylint = "~=2.14.0"
pylint_junit = ">=0.3.2"
toml-sort = ">=0.20.1"

[tool.poetry.group.test.dependencies]
numpy = "^1.19"
pytest = "^7.1.0"
pytest-bdd = "^6.1.0"
pytest-cov = "^4.0.0"

[tool.poetry.group.doc.dependencies]
markupsafe = "^2.1"
pygments = ">=2.13"
recommonmark = ">=0.7.1"
sphinx = ">=5.3.0"
sphinx-autobuild = ">=2021.3.14"
sphinx_pyproject = ">=0.1.0"
sphinx_rtd_theme = ">=1.1.1"
sphinxcontrib-websupport = ">=1.2.4"
enum_tools = { version = "^0.9.0", extras = ["sphinx"] }

[tool.pylint.MASTER]
ignore = 'version.py'

[tool.pylint.BASIC]
# Don't require docs on private or test methods/classes
no-docstring-rgx = '^_|^test_|^Test'

[tool.pylint.SIMILARITIES]
ignore-imports = 'no'

[tool.pylint.TYPECHECK]
ignored-classes = [
    'optparse.Values',
    'thread._local',
    '_thread._local',
    'astropy.units',
]

[tool.pylint.MISCELLANEOUS]
notes = ['FIXME', 'XXX']

[tool.pylint.'MESSAGES CONTROL']
disable = [
    'too-few-public-methods',
    'too-many-arguments',
    'too-many-locals',
    'too-many-instance-attributes',
    'too-many-public-methods',
    'too-many-return-statements',
    'too-many-statements',
    'duplicate-code',
    'no-else-return',
    'no-else-continue',
    'redefined-outer-name',
    'invalid-name',
    'missing-module-docstring',
    # We handle this with isort
    'wrong-import-order',
    'ungrouped-imports',
    # Black sometimes allows lines that are longer (e.g. to avoid breaking string)
    'line-too-long',
]

[tool.isort]
profile = "black"
line_length = 79
